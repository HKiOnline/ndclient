# ndclient
Narrative Dice CLI for FFG Star Wars Roleplaying Game.
Pass the dice you which to roll as flag arguments to the CLI.

Download ready built [binaries for your platform](https://bitbucket.org/HKiOnline/ndclient/downloads).


```
usage: ndclient [<flags>]

Flags:
--help                Show context-sensitive help (also try --help-long and --help-man).
-a, --ability=0           Number of Ability-dice in the roll
-p, --proficiency=0       Number of Proficiency-dice in the roll
-b, --boost=0             Number of Boost-dice in the roll
-d, --difficulty=0        Number of Difficulty-dice in the roll
-c, --challenge=0         Number of Challenge-dice in the roll
-s, --setback=0           Number of Setback-dice in the roll
-f, --force=0             Number of Force-dice in the roll
-%, --percentile          Roll Percentile-dice
-o, --percentileOffset=0  Set percentile offset
-r, --results             Show condensed result collection instead of the text summary
```

## Example
```
ndclient -a 2 -p 1 -b 1 -d 2 -s 1
```

## Result format
Result can be displayed as a text summary or as a simple collection.
The text summary is more user friendly but the collection format is better if you are planning to pass the result to another utility.

Text summary:
```
• 2 sucesses
• 1 threats
• 1 triumphs
```

Collection:
```
{Success' Advantages Triumphs Failures Threats Despairs Light-points Dark-point Percentile-result}
```
An example of this would be:
```
{4 3 0 1 2 0 0 0 0}
```
