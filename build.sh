#!/bin/sh
APP_NAME="ndclient"

env GOOS=darwin GOARCH=amd64 go build -v
mkdir -p bin/mac/; mv $APP_NAME bin/mac/

env GOOS=linux GOARCH=amd64 go build -v
mkdir -p bin/linux/; mv $APP_NAME bin/linux/

env GOOS=windows GOARCH=amd64 go build -v
mkdir -p bin/windows/; mv $APP_NAME".exe" bin/windows/

cp README.md bin/mac/README.md
cp README.md bin/linux/README.md
cp README.md bin/windows/README.md

mkdir -p zip/
zip -r zip/all-binaries.zip bin/
zip -r zip/mac-binaries.zip bin/mac/
zip -r zip/linux-amd64-binaries.zip bin/linux/
zip -r zip/windows-amd64-binaries.zip bin/windows/
