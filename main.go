package main

import "fmt"
import dice "bitbucket.org/HKiOnline/ndlib"
import cmd "gopkg.in/alecthomas/kingpin.v2"

var (
  abilityDices = cmd.Flag("ability", "Number of Ability-dice in the roll").Default("0").Short('a').Int()
  proficiencyDices = cmd.Flag("proficiency", "Number of Proficiency-dice in the roll").Default("0").Short('p').Int()
  boostDices = cmd.Flag("boost", "Number of Boost-dice in the roll").Default("0").Short('b').Int()

  difficultyDices = cmd.Flag("difficulty", "Number of Difficulty-dice in the roll").Default("0").Short('d').Int()
  challengeDices = cmd.Flag("challenge", "Number of Challenge-dice in the roll").Default("0").Short('c').Int()
  setbackDices = cmd.Flag("setback", "Number of Setback-dice in the roll").Default("0").Short('s').Int()

  forceDices = cmd.Flag("force", "Number of Force-dice in the roll").Default("0").Short('f').Int()

  percentileDice = cmd.Flag("percentile", "Roll Percentile-dice").Default("false").Short('%').Bool()
  percentileOffset = cmd.Flag("percentileOffset", "Set percentile offset").Default("0").Short('o').Int()

  showResultArray = cmd.Flag("results", "Show condensed result collection instead of the text summary").Default("false").Short('r').Bool()
)


func main() {

  cmd.Parse()

  dp := dice.Dicepool{
  Ability: *abilityDices,
  Proficiency: *proficiencyDices,
  Boost: *boostDices,
  Difficulty: *difficultyDices,
  Challenge: *challengeDices,
  Setback: *setbackDices,
  Force: *forceDices,
  Percentile: *percentileDice,
  PercentileOffset: *percentileOffset}

  result := dice.Roll(dp)
  summary := dice.TextSummary(result)

  if *showResultArray {
    fmt.Println( result )
  } else {
    fmt.Println( summary )
  }

}
